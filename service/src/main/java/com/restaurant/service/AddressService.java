package com.restaurant.service;

import com.restaurant.dao.AddressDao;
import com.restaurant.domain.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    private AddressDao addressDao;

    public List<Address> getAddresses(){

        return (List<Address>)addressDao.findAll();
    }

}
