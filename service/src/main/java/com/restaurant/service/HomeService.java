package com.restaurant.service;

import org.springframework.stereotype.Service;

@Service
public class HomeService {

    public String getData(){
        return "Welcome message from Service layer";
    }
}
