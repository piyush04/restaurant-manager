package com.restaurant.controller;

import com.restaurant.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/home")
public class HomeController {


    @Autowired
    private HomeService homeService;

    @GetMapping
    public String getHome() {
        return homeService.getData();
    }
}
